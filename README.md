# Color Palletes
My collection of color palettes I collected over the years. Feel free to use them as you wish.

## Where Did I Get These?
I find color palettes in a number of different locations but good places to check out include [color-hex](https://www.color-hex.com/color-palettes/), [colorhunt](https://colorhunt.co/) and [colorpalettes](https://colorpalettes.net/). Some of them are made by myself.

## Ownership
Because I downloaded most of these palletes images from sites like color-hex, colorhunt and colorpalettes, I have no way of knowing if there is a copyright on these images. If you find an image hosted in this repository that is yours and of limited use, please let me know and I will remove it.
